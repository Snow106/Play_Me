
// vars 
var value, maxTime, XX, YY, inner_r, outer_r, dlvl, angleStart, angleMax, dir, texture, cAngle

value  = argument0 // the value ( mana or hp )
maxTime = argument1 // the max time
XX  = argument2 // x position
YY  = argument3 // y position
inner_r  = argument4 // inner radius
outer_r  = argument5 // outer radius
dlvl = argument6 // level of details i recommend 30 
angleStart = argument7 // starting angle for drawing the Hb
angleMax = argument8 // maximum angle in case you want to make a half round
dir = argument9 //direction
texture = background_get_texture(argument10) // the texture 
cAngle =value*angleMax/maxTime // converting the value to an angle 

dir = 1;
//draw
draw_set_color(c_white) // so we don't have black health bar 
draw_primitive_begin_texture(pr_trianglestrip,texture)

for (i=0;i<=dlvl ;i+=1)
    {
        draw_vertex_texture(XX + lengthdir_x(inner_r,angleStart + (cAngle*i/dlvl)),YY + lengthdir_y(inner_r,angleStart + (cAngle*i/dlvl)),0,0) // inner part
        draw_vertex_texture(XX + lengthdir_x(outer_r,angleStart + (cAngle*i/dlvl)),YY + lengthdir_y(outer_r,angleStart + (cAngle*i/dlvl)),1,1) // outer part
    }
draw_primitive_end()
