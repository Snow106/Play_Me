//Destroy non live blocks.
with (obj_block)
{
    if (x < 545)
    image_speed = 1;
}

//wave1 

switch (obj_overload.wave)
{
    case 1:     
                //spawn lives
                var i;
                for (i = 0; i<5; i++)
                {
                    script_execute(scr_spawnInGrid, "left", 8, i, obj_heart)
                }
                
                script_execute(scr_spawnInGrid, "right", 0, 1, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 0, 2, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 0, 3, obj_enemy)
                waveSpeed += 2;
                break;
                
    case 2:     script_execute(scr_spawnInGrid, "right", 0, 0, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 0, 4, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 1, 1, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 1, 2, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 1, 3, obj_enemy)
                waveSpeed += 2;
                break;
                
    case 3:     script_execute(scr_spawnInGrid, "right", 1, 0, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 2, 1, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 3, 2, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 2, 3, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 1, 4, obj_enemy)
                waveSpeed += 3;
                break;
                
    case 4:     script_execute(scr_spawnInGrid, "right", 2, 0, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 2, 2, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 2, 4, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 3, 1, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 3, 3, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 4, 0, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 4, 4, obj_enemy)
                waveSpeed += 3;
                break;
                
    case 5:     script_execute(scr_spawnInGrid, "right", 3, 0, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 3, 4, obj_enemy)  
                script_execute(scr_spawnInGrid, "right", 4, 1, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 4, 2, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 4, 3, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 5, 0, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 5, 4, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 6, 0, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 6, 4, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 7, 0, obj_enemy)
                script_execute(scr_spawnInGrid, "right", 7, 4, obj_enemy)
                
                waveSpeed += 4;
                //boss block
                {
                    script_execute(scr_spawnInGrid, "right", 6, 2, obj_bossBlock)
                }
                break;
                
   case 6:     obj_bossBlock.active = 1;
                break;
}
