//create
squares = part_type_create();

//settings
part_type_shape(squares,pt_shape_square);
part_type_size(squares,0.10,0.10,0,0);
part_type_scale(squares,2,2);
part_type_color1(squares,$ffffff);
part_type_alpha1(squares,1);
part_type_speed(squares,0,0.50,0.10,0);
part_type_direction(squares,0,359,0,0);
part_type_gravity(squares,0.01,270);
part_type_orientation(squares,0,0,0,0,1);
//part_type_blend(squares,1);
part_type_life(squares,40,40);


